#### <sup>:closed_book: [wiki](../README.md) → Un site statique</sup>

---

## Un site statique

### Qu'est-ce qu'un site statique

Il faut imaginer un site internet comme plusieurs morceaux épars. Par exemple, le contenu et les gabarits de mise en pages sont distincts. Ils sont deux dossiers séparés, tout deux impropres à une navigation sur le web. 
Leurs densités ne sont pas identiques. Quand le contenu peut consister en plusieurs centaines de pages et de fichiers images, les gabarits ne rassemblent eux que deux ou trois modèles types.
Comparons les gabarits à des moules à gâteau. Lorsque nous souhaitons générer une version navigable et diffusable du site, nous lancerons un processus dit "de construction" qui coulera chaque page de contenu dans le moule correspondant. Il en résultera autant de pages web navigables et mises en page.

C'est ici que se démarque un site dit statique. Celui-ci est généré une fois pour toute quelque que soit le nombre de visiteurs. Il se démarque d'un site dit dynamique qui lui génère le site autant de fois qu'il est visité. 

Les atouts principaux de cette technique réside en un gain de vitesse à l'ouverture de la page et en une économie d'énergie sur la durée ( à tâche égale, mieux vaut une machine qui travaille une fois pour mille autre que mille machines travaillant chacunes pour elles-même) 


### Gitlab ?

Le site internet est hébergé sur un serveur tiers, celui de Gitlab. Cette solution couple :

+ un logiciel dit de _versioning_ (travailler en collaboration sur des fichiers, établir des sauvegardes régulières de son travail)
+ une solution d'hébergement (stocker les fichiers de travail et le site internet à proprement parler)
+ un générateur de site statique, Hugo.

Pour dire les choses simplement, nous créons, éditons, échangeons des fichiers textes et téléversons des images par intermédiaire d'un [dossier en ligne](https://gitlab.com/MarineKaiser/margaret). A chaque mise à jour de ce dossier, le générateur de site statique compilera l'ensemble des fichiers pour créer des pages web navigable en HTML. Ces pages seront hébergées sur Gitlab. L'adresse [marinekaiser.ch]() renverra vers ce serveur. 


### Git ? 

Git est un logiciel libre de travail collaboratif. Sans rentrer dans le détail de ce logiciel issu du monde de la programmation, évoquons simplement un de ses principes de base pour contribuer à un travail commun, le _commit_. Ce terme renvoie au même sens que le français _commettre_, auquel se rajoute le sens de _confier quelque chose_. Le _commit_ est donc un Apport.

L'édition d'un fichier en ligne sur l'interface Gitlab, tout comme l'édition via son serveur local, donneront lieu à un _commit_.
La seconde méthode permettant un peu plus de flexibilité.

