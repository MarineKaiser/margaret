# Tutoriel


- [Un site statique](./un_site_statique.md)

- [Choisir sa méthode pour éditer le site](./methode.md)

- Editer le site

  - [Structure générale des projets](./structure_generale.md)

  - [Editer une fiche projet](./fiche_projet.md)

  - [Editer la biographie](./biographie.md)

- [Sauvegarder une archive du site en cours](./sauvegarde.md)


## Aller plus loin

Le générateur du site Hugo est très flexible. Il sera facile de le reprendre en main pour, à partir de la même base de projet, tendre vers un autre site web. Il est par exemple possible de créer une [_branche_](https://docs.gitlab.com/ee/gitlab-basics/create-branch.html) (alias une déclinaison de travail) ou de [_cloner_](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html) l'entièreté du répertoire. 

En d'autres termes, Marine, tu disposes désormais du produit de notre travail. Tu pourras facilement faire procéder à des évolutions ou révolutions avec d'autres personnes. Il s'agira de travailler "à partir d'un site statique généré avec *Hugo*".

## Annexes

- [Tutoriel pour utilisation de GitLab](./annexe-gitlab.md)

- [Tutoriel pour utilisation de Markdown](./annexe-markdown.md)

