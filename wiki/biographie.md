#### <sup>:closed_book: [wiki](../README.md) → Editer le site → Editer la biographie</sup>

## Editer le site

### Editer la biographie

Il s'agit de modifier le fichier `about.md` de la même manière que les fiches projets.
Entrer une nouvelle exposition ou résidence se fait dans une syntaxe similaire à celle des bulles.

```
[[ exposition ]]  
	date = "2018-06-01"
	titre = "Leste, Across the Border"
	lieu =	 "Palazzo Ajutamicristo, Manifesta 12"
	ville = "Palerme (IT)"
```