#### <sup>:closed_book: [wiki](../README.md) → Choisir sa méthode pour éditer le site</sup>

---

## Choisir sa méthode pour éditer le site

Il existe plusieurs méthodes pour éditer les fichiers.

### via l'editeur en ligne 

La solution la plus rapide et simple pour commencer l'édition de ce site web est de recourir à l'éditeur en ligne de Gitlab.
Une [annexe](/annexe-gitlab.md) est dédiée à l'utilisation de cette méthode en ligne.

### via un serveur local ( travailler hors ligne sur son propre ordinateur )

Elle permet un gain de temps ( momentanément débarrassé des performances du site Gitlab ou de la vitesse de sa connexion internet), un gain d'énergie (on ne sollicite pas un serveur en ligne intempestivement ) et une meilleure navigation entre les fichiers.

La marche à suivre se déroule ainsi. (Les détails seront à trouver sur un moteur de recherche)

La première fois :

+ Installer Git où [Github desktop ](https://help.github.com/en/desktop/getting-started-with-github-desktop) sur son ordinateur. Le second logiciel est plus simple à utiliser.
+ Installer [hugo_extended](https://github.com/gohugoio/hugo/releases) sur son ordinateur
+ Cloner les fichiers du répertoire en ligne sur son ordinateur (via [git](https://help.github.com/en/articles/cloning-a-repository) ou [github desktop](https://itnext.io/how-to-use-github-desktop-with-gitlab-cd4d2de3d104) )

Au quotidien, éditer un fichier :

+ Ouvrir le fichier ciblé avec un éditeur de texte ( comme Sublime, ou Visual Studio)
+ Sauvegarder les changements
+ _Commiter_ les changements
  + via le terminal si Git est installé
  + via le logiciel Git Desktop si celui-ci est installé

Au quotidien, observer ces changements ( processus distinct, indépendant mais complémentaire au précédent )

+ Atteindre le répertoire local des fichiers dans un terminal de commande `cd chemin/vers/le/repertoire`
+ Lancer le générateur Hugo `hugo serve`
+ Dans le navigateur atteindre le site généré localement pour observer les modifications `localhost:1313`
