#### <sup>:closed_book: [wiki](../README.md) → Sauvegarder une archive du site en cours</sup>

## Sauvegarder une archive du site en cours

Il est très facile de sauvegarder l'ensemble des fichiers du site. 
Dans l'interface Gitlab, à la racine du projet, il suffit de télécharger le dossier :

![Download the sources](https://docs.gitlab.com/ee/user/project/repository/img/download_source_code.png)