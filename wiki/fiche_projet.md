#### <sup>:closed_book: [wiki](../README.md) → Editer le site → Editer une fiche projet</sup>

## Editer le site

### Editer une fiche projet

#### Généralité & syntaxe : Ligne, Tableau, Multiligne... la bonne formulation pour chaque champs

Ce chapitre donne les trois règles générales qui seront utiles à la constitution du *frontmatter* (cf. chapitre suivant)

##### Ligne (String) une valeur linéaire simple

	titre = `Un titre`


##### Tableau (Array), un tableau de données

Une liste de valeurs définies par des guillemets anglais, séparées par des virgules, englobées dans des crochets.

```
  ["01", "02", "03"]`
	
  ["Pierre", "Papier", "Ciseaux rouge"]
 
  [
      "Nadine",
      "Elaey", 
      "Patty"        
  ]
```


##### Valeurs imbriquées

Des valeurs imbriquées les unes dans les autres façon Poupées russes
On les reconnait à leurs doubles crochets, suivies à la ligne d'une valeur *String* ou *Array*.
Cette valeur est décalée par une tabulation. Nous utiliserons cette forme notamment pour les bulles


	[[legende]]
		date = ""
		image = ["01", "02"]







#### Chapeau introductif essentiel à chaque fiche projet (alias _Frontmatter_) 

Toutes les fiches projets débutent par un _frontmatter_, un chapeau introductif

```
  +++
  draft = false
  date = "2016-06-01"

  type = "projet"
  layout = "plusieurs_carrousels"
  map = true

  title = "Un Titre"
  id = "un_titre"

  lieu = []
  collaboration_avec = []
  commanditaire =  []
  credit = ""
  remerciement = ""
  performeurs = ""

  description_projet = ""
  mot_cle_projet = []
  auteur_du_projet = []


  [[legende]]
    image = ["01"]
    contexte = ["information", "information deux"]
    description = " Une longue decription <br> Avec plusieurs lignes"

  +++

  Ici le texte de l'article
  
```

Ce _frontmatter_ est un regroupement d'informations para-texte. Des informations qui ne font pas pleinement partie du contenu du projet mais qui sont utiles pour le contextualiser ( titre, date, id...). Certaines informations sont essentielles, d'autres contingentes.
Enfin, une seconde façon de décrire le _frontmatter_ est de le décrire comme une poignée d'informations organisées et formatées pour être lues par un script.

Un exemple, dire : 

> "Le projet A fut réalisé le 10 février 2016, quand le B fut réalisé un mois aprés"

Le script aura un peu de mal à comprendre que nous décrivons deux projets, réalisés à un mois d'intervalle

Toutefois écrire : 

    [[Projet A]]
        date = "2016-02-10"
    
    [[Projet B]]
        date = "2016-03-10"

Ici le script, comprendra.

A noter le _frontmatter_ débute et se termine par `+++`. C'est une façon arbitraire de le délimiter. 

##### Quels sont les paramètres obligatoires

A partir de l'exemple vu précédemment :

```
  # Le projet est-il un brouillon (true) ou un projet publié (false)
  draft = false 

  # La date suivant le format Année-Mois-Jour         
  date = "2016-06-01"
  
  # Cette valeur ne change pas dans le cas d'une fiche projet
  type = "projet"

  # Définir le gabarit. Choix entre 'plusieurs_carrousels' et 'un_carrousel'
  layout = "plusieurs_carrousels" 
  

  # Définir si le projet est dans la barre latérale : si il s'agit d'une pièce incluse au sein d'un gabarit 'plusieurs_carrousels' alors la valeur est false.
  map = false

  # Ce paramètre peut être rédigé normalement
  title = "Un Titre"
  
  # Ce paramètre peut être rédigé comme un robot. Cette id doit être le même que celui des images
  id = "un_titre_a_valeur_d_identifiant"
```

A noter le *frontmatter* débute et se termine par `+++`

Par commodité et pour éviter une trop longue rédaction, il est possible de dupliquer une fiche projet existante et de l'éditer.






#### Contenu d'un article

A la suite de l'austère _frontmatter_ et avant d'évoquer la partie Image, parlons du contenu texte de chaque projet. Cette partie n'est pas obligatoire et vient après le `+++` de fin du _frontmatter_. Elle peut être rédigée normalement ou en _markdown_, une syntaxe permettant d'enrichir le texte d'italic, de gras, de liens... Sur ce point, voir la section Markdown en annexe.

Chaque fiche projet est ponctuée par cette partie. Voyons désormais comment lier des images au projet, puis leur attribuer une "bulle"

#### Les images et vidéos

Les formats supportés sont Jpg et Png pour les images et Mp4, Mpeg, Webm pour les videos.

##### Former un titre pour chaque média

Cela se fait suivant la logique suivante, deux à trois informations séparées par un tiret.

`id-01.jpg`

Donc 

`un_titre_a_valeur_d_identifiant-01.jpg`

`un_titre_a_valeur_d_identifiant-02-si_besoin_de_precision.jpg`

Attention les numéros de l'image conditionnent leur ordre d'apparition
Attention, les numéros 1 à 9 commencent tous par un 0
01, 02, 03, ...

Idem pour une vidéo

`un_titre_a_valeur_d_identifiant-01.mp4`


##### Téléverser une image

Il suffit de placer les images ainsi nommées à cet emplacement `website/content/media_library`.
Il est possible d'effectuer cette opération via l'interface en ligne Gitlab ou directement en local depuis son ordinateur.


#### Les bulles

Créer une légende en lien avec une image ( alias une bulle) se fait dans le _frontmatter_ de la fiche projet.
Il convient de rajouter les lignes suivantes :

  ```
	[[legende]]
        image = ["01"]
        contexte = ["information", "information deux"]
        description = " Une longue decription <br> Avec plusieurs lignes"
  ```

En détail, celle-ci stipule que :

+ Que nous ajouterons une légende...

+ ...pour l'image 1  *→ image = ["01"]*

+ ... avec des éléments contextualisant  *→ contexte = ["a", "b"]*

+ ... et quelques paragraphes de texte libre  *→ description = "abc"*



Chaque nouvelle légende doit être réintroduite par`[[legende]]`
Ce champ image peut être occupé par plusieurs numéros d'images (ex: `image = ["01", "02", "03"]` ). La légende s'appliquera alors à toutes les images contenues dans cet *array*.

##### Indentation (Tabulation)

Il convient de respecter l'indentation
Par exemple, si je souhaite introduire un retour à la ligne dans le champ description d'une image légendée:

Mauvais exemple !

	[[legende]]
	    description = " Une longue decription 
	avec plusieurs lignes"

Ci dessus, le mot "Avec" rompt la structure sémantique. Le robot ne comprendra pas la nature linéaire de ce champ. Préférons l'exemple ci-dessous. La balise `<br>` vient indiquer un saut de ligne.

	[[legende]]
	    description = " Une longue decription <br> avec plusieurs lignes"
