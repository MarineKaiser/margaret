
## Annexe : Markdown

### Heading	

    # H1
    ## H2
    ### H3

### Bold 	

    **bold text**
    

### Italic 	

    *italicized text*
    

### Blockquote 	

    > blockquote
    

### Ordered List 

    1. First item
    2. Second item
    3. Third item
    

### Unordered List 	- First item

    - Second item
    - Third item
    

### Code 	

    `code`
    

### Horizontal Rule 	

    ---
    
    
### Link 	

    [title](https://www.example.com)


## Erreur courante

Des erreur de typo'. Par exemple : 
    
    array = [01", "02"]            // Manque un guillemet avant le 01
    array = [                      // Manque des virgule entre les valeurs
        "01"
        "02"
    ]
