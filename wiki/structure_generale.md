#### <sup>:closed_book: [wiki](../README.md) → Editer le site → Structure générale des projets</sup>

## Editer le site

### Structure générale des projets

Le site est composé d'une multitude de fiches projets. Celles-ci se trouvent dans le répertoire `website/content/project` du site.

Aparté, cet emplacement se distingue par exemple de `website/content/media_library` emplacement où nous retrouverons l'ensemble des images de **tous** les projets.

#### Une fiche projet

Une fiche projet se nomme ainsi `le_titre_de_mon_projet.md`.

La première partie est évidemment personnalisable. Chaque mot est séparé d'un *underscore* `_` . Les accents, apostrophe et guillemets sont à proscrire, tout comme les *tirets*. nous nommerons ce nom ID.

La seconde partie `.md`désigne l'extension de la fiche projet ( ici markdown). Cette extension n'a pas vocation à être changée sauf dans le cas particulier d'un groupement de plusieurs fiches projets.

Exemple de nom d'une' fiche projet : `cote_jardin.md`

#### Faire un groupe de plusieurs fiches projets 

Pour regrouper plusieurs pièces en une seul page contenant plusieurs pièces, il convient de créer un dossier avec comme nom un ID symbolisant l'ensemble des pièces qu'il contient. Lorsque nous créons un dossier, il est inutile de désigner une extension comme _.md_, un dossier étant par définition un regroupement de fichiers.

Exemple de nom de dossier : `la_ballade_de_kaiser_et_qasimov`

A l'intérieur de ce groupe, place ou créer la fiche projet suivant la procédure décrite au paragraphe précédent.

Enfin, il faudra créer un fichier `_index.md`, sorte de fiche projet globale.
Celle-ci contient un _frontmatter_ et un texte d'introduction à tous les projets.
C'est la date contenue dans le _frontmatter_ de cet `_index.md` qui sera prise en compte pour définir la place de ce groupe dans la liste latérale.

#### Exemple de structure générale des projets

Dans cet exemple, une tabulation marque une imbrication

```
Project
  lat.md
  les_moules.md
  la_ballade_de_kaiser_et_qasimov
    cote_jardin.md
    le_confort.md
  suzanne.md
```


#### Créer une fiche projet

Passons maintenant à l'édition détaillée de cette fiche projet.