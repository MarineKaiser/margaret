+++
date = "2018-01-01"
draft = false

type = "about"
layout= "about" 

title = "A propos de Marine Kaiser"
id = "about"
url = "about.html"

[[ exposition ]] 
	date = "2023-03-30"
	titre = "climat favorable"
	lieu = "Espace Contact"
	ville = "Neuchâtel (CH)"

[[ exposition ]] 
	date = "2022-12-01"
	titre = "Grandmotherhood (uncountable)"
	lieu = "Baba Vasa Cellar x Laurenz"
	ville = "Shabla (BG)"

[[ exposition ]] 
	date = "2022-12-01"
	titre = "Open Studios"
	lieu = "Jan van Eyck Academie"
	ville = "Maastricht (NL)"

[[ exposition ]] 
	date = "2022-12-01"
	titre = "The only thing that can affect magnetic inversion is bad news"
	lieu = "RECLAME"
	ville = "Maastricht (NL)"

[[ exposition ]] 
	date = "2021-12-01"
	titre = "Doppelgänger"
	lieu = "KANAL-Centre Pompidou"
	ville = "Bruxelles (BE)"

[[ exposition ]]  
	date = "2021-12-01"
	titre = "Stuff & Feelings"
	lieu = "Les Basseurs"
	ville = "Liège (BE)"

[[ exposition ]]  
	date = "2021-12-01"
	titre = "Nostalgia and Bombast"
	lieu = "Salle Crosnier"
	ville = "Genève (CH)"

[[ exposition ]]  
	date = "2020-12-01"
	titre = "Adjustments Would Be Made According to Seasonal and Diurnal Patterns of the Sun"
	lieu = "CHEE"
	ville = "Bruxelles (BE)"

[[ exposition ]]  
	date = "2020-12-01"
	titre = "Prix Médiatine"
	lieu = "La Médiatine"
	ville = "Bruxelles (BE)"

[[ exposition ]]  
	date = "2019-12-01"
	titre = "Don't Worry about the Government"
	lieu = "Cunst-Link, Golden Night Shop, Brico Barrière"
	ville = "Bruxelles (BE)"

[[ exposition ]]  
	date = "2018-12-01"
	titre = "Stuff & Feelings"
	lieu =	 "LE 18"
	ville = "Marrakech (MA)"

[[ exposition ]]  
	date = "2018-06-01"
	titre = "Leste, Across the Border"
	lieu =	 "Palazzo Ajutamicristo, Manifesta 12"
	ville = "Palerme (IT)"


[[ exposition ]]  
	date = "2018-06-01"
	titre = "Les Moules, Editions Cacahuète"
	lieu =	 "Association pour l'estampe & l'art populaire"
	ville = "Paris (FR)"


[[ exposition ]]  
	date = "2018-04-01"
	titre = "I Would Prefer to, Makeazinescene II"
	lieu =	 "Onomatopee"
	ville = "Eindhoven (NL)"


[[ exposition ]]  
	date = "2018-02-01"
	titre = "Coop"
	lieu = "La Placette"
	ville = "Lausanne (CH)"


[[ exposition ]]  
	date = "2017-11-01"
	titre = "Chéri, Scénographie et technologie II"
	lieu =	 "Les Grands-Voisins"
	ville = "Paris (FR)"


[[ exposition ]]  
	date = "2017-09-01"
	titre = "LAT"
	lieu =	 "Vernissage des Bourses de la ville, Centre d’art contemporain"
	ville = "Genève (CH)"


[[ exposition ]]  
	date = "2017-05-01"
	titre = "Mutual Attraction : Bisou, Feed your friends"
	lieu =	 "ODD"
	ville = "Bucarest (RO)"


[[ exposition ]]  
	date = "2017-05-01"
	titre = "La Présentation"
	lieu =	 "Bmw brand store (pirate)"
	ville = "Bruxelles (BE)"

[[ exposition ]]  
	date = "2017-05-01"
	titre = "de printemps"
	lieu =	 "Podium, atelier Bek"
	ville = "Bruxelles (BE)"


[[ exposition ]]  
	date = "2017-04-01"
	titre = "de printemps"
	lieu =	 "L’Espace du débat, Pavillon de l’Arsenal"
	ville = "Paris (FR)"

[[ exposition ]]  
	date = "2017-04-01"
	titre = "Le Retard"
	lieu =	 "Festival Poisson-Evêque, Le Maga"
	ville = "Bruxelles (BE)"


[[ exposition ]]  
	date = "2017-04-01"
	titre = "Mutual Attraction: Le Bisou"
	lieu =	 "Friche/les Hangars"
	ville = "Bruxelles (BE)"


[[ exposition ]]  
	date = "2017-04-01"
	titre = "Des nouvelles de CK"
	lieu =	 "Bootleg DAF festival OFF"
	ville = "Genève (CH)"


[[ exposition ]]  
	date = "2016-09-01"
	titre = "In dieser Bibliothek lesen Menschen laut"
	lieu =	 "Public Library, AGB"
	ville = "Berlin (DE)"

[[ exposition ]]  
	date = "2016-06-01"
	titre = "Répétition Commentaire"
	lieu =	 "erg "
	ville = "Bruxelles (BE)"


[[ exposition ]]  
	date = "2016-05-01"
	titre = "Répétition Commentaire"
	lieu =	 "Les Gates, 019"
	ville = "Gand (BE)"


[[ exposition ]]  
	date = "2015-06-01"
	titre = "…juste pour voir (That’s it), Sunny side up"
	lieu =	 "Netwerk"
	ville = "Alost (BE)"


[[ exposition ]]  
	date = "2015-01-01"
	titre = "Pratiquer l’art"
	lieu =	 "Cafétéria du Wiels (invitation personnelle)"
	ville = "Bruxelles (BE)"

[[ exposition ]]  
	date = "2014-06-01"
	titre = "Rideau!"
	lieu =	 "BIVOUAC, VVV et 37° 05′ N / 2°30′ W, Villa du parc"
	ville = "Annemasse (FR)"


[[ exposition ]]  
	date = "2014-06-01"
	titre = "OK"
	lieu =	 "@ptt"
	ville = "Genève (CH)"


[[ exposition ]]  
	date = "2014-02-01"
	titre = "Magari"
	lieu =	 "Rewind the bocs (revox), BOCS"
	ville = "Catane (IT)"


[[ exposition ]]  
	date = "2013-11-01"
	titre = "Rideau!"
	lieu =	 "Pièce montée, Projektraum Bethanien"
	ville = "Berlin (DE)"

[[ exposition ]]  
	date = "2013-05-01"
	titre = "Crossing paths"
	lieu =	 "Tunneling art and science, Temple Bar Gallery"
	ville = "Dublin (IR)"

[[ residence ]]  
	date = "2022-12-01"
	lieu = "Jan van Eyck Academie"
	ville = "Maastricht (NL)"


[[ residence ]]  
	date = "2018-12-01"
	lieu = "LE 18"
	ville = "Marrakech (MA)"

[[ residence ]]  
	date = "2017-05-01"
	lieu = "OFF Printemps de l’art contemporain"
	ville = "Marseille (FR)"

[[ residence ]]  
	date = "2017-04-01"
	lieu = "Friche/les Hangars  "
	ville = "Bruxelles (BE)"

[[ residence ]]  
	date = "2016-05-01"
	lieu = "019"
	ville = "Gand (BE)"

[[ residence ]]  
	date = "2015-06-01"
	lieu = "Netwerk"
	ville = "Alost (BE)"

[[ residence ]]  
	date = "2013-11-01"
	lieu = "Radio Pic-Nic"
	ville = "Berlin (DE)"

+++

Marine Kaiser est née en 1992 comme l’album Uh-Oh. Marnie Kasier est franco-suisse comme le soufflé au Grand-Marnier. MK a fréquenté la HEAD-Genève, le NCAD-Dublin et l’erg-Bruxelles. En France comme en Suisse comme en Italie comme en Irlande comme en Allemagne comme en Belgique Maaki Sineerr a montré son travail. Marine/Marin vit à Bruxelles/Brussel.  
