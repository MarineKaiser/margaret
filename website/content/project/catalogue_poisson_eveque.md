+++
draft = true
date = "2017-06-01"

type = "projet"
layout= "un_carrousel" 
map = true

title = "Poisson-Evêque"
id = "catalogue_poisson_eveque"

lieu = [ ]
collaboration_avec = [ "Romain Marula", "Etienne Ozeray", "Stéphanie Verin" ]
commanditaire =  [ "Dans le cadre du festival Poisson-Evêque" ]
credit = ""
remerciement = ""
performeurs = ""


description_projet = ""
mot_cle_projet = []
auteur_du_projet = []
+++

Catalogue  du festival de performances Poisson-Evêque curaté par Stéphanie Verin ayant prit place au Maga, Bruxelles du 27 au 31 avril 2017.
Avec des propositions de Irina Favero-Longo et Rebecca Komforti, Jot Frau, André D. Chapatte et Stephan Blumenschein, Clément Thiry, Bettina Kordjani, Stéphanie Becquet et Romy Vanderveken et Marjolein Guldentops, Otto Byström, Antoine Loyer et Sandra Naji, Bernadette Mayer, Rares Craiut et Xavier Gorgol, Pop (potential office project), Maxime Lacôme, William Lambeau, Marine Kaiser et Stéphanie Verin.




