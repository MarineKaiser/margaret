+++
draft = false
date = "2023-03-30"

type = "projet"
layout= "un_carrousel" 
map = true

title = "climat favorable"
subtitle = []
id = "climat"

lieu = "Espace Contact, Neuchâtel (CH)"
collaboration_avec = []
commanditaire =  []
credit = "Photography : Caroline Bourrit"
remerciement = "Thank you Caroline Bourrit, Ahmad Motalaei, Manuel Perrin, Alexis, Mathias C. Pfund"
performeurs = ""
+++

In the telephone booths of Place Pury, I placed a brown envelope woven from quagga mussel shells (classified as invasive in Western Europe) and a cowrie shell (secular currency brutally devalued by colonial hubris). The ground is slipping away while the presence of a black box that collects the surrounding sound information emerges.
On the Contact line +41 32 544 68 68, you listen to an extract from The Conversation (Francis Ford Coppola, 1974) : "I think he’s been recording my telephone ».