+++
draft = false
date = "2021-11-29"

type = "projet"
layout= "un_carrousel" 
map = true

title = "GALA"
subtitle = []
id = "gala"

lieu = []
collaboration_avec = []
commanditaire =  []
credit = "Photography : Greg Clément"
remerciement = ""
performeurs = ""



+++

Bringing together two contemporary practices: the export trade of second-hand vehicles and the departure of European retirees to Africa, Gala presents a 3D video where the fiction of a Ro-Ro cargo sailing to an elsewhere builds up in the crossing of a still scene annotated with found images. 
On the walls, the business cards used for European canvassing of second-hand vehicles sold in Africa are preserved between two glasses engraved with logos re-enacting various standards of goods’ international circulation. 
