+++
draft = false
date = "2022-04-29"

type = "projet"
layout= "un_carrousel" 
map = true

title = "There Really Is"
subtitle = []
id = "lock"

lieu = []
collaboration_avec = []
commanditaire =  []
credit = "Photography : Jan van Eyck Academie"
remerciement = ""
performeurs = ""



+++

There Really Is is the temporary title of the piece. Reflecting on society, this chromed truck lock follows political assertions and evolves over time. Thatcher’s « No such thing as society » has been contradicted in 2020 by Boris Johnson during his Covid-19 quarantine. « There really is such a thing as society » he asserted. The lock, as a metonymy of the 3.5T white truck regularly operating for informal activities such as temporary contracts or daily runs interrogates a collaborative presence through its reflective surface.
