+++
draft = false
date = "2022-06-30"

type = "projet"
layout= "un_carrousel" 
map = true

title = "The Assistant"
subtitle = []
id = "assistant"

lieu = []
collaboration_avec = []
commanditaire =  []
credit = "Photography : Jan van Eyck Academie"
remerciement = "Thanks to Bruno Alves de Almeida , Hicham Khalidi, Jonas Van de Vosse, Ron Bernstein, Hamid Soleymani, Romy Finke, Margriet Thissen, Claudia Bos, Karin Rijpkema, Marente van der Valk, Amanda Sarroff, Sereina Rothenberger, Gregory Castera, Lev Bratishenko, the JvE team and it’s volunteers, Charly Hermanns, Mathias Pfund, Adeline Molle and all my fellow JvE friends"
performeurs = ""



+++

The Assistant (2022) radiates around two points of reference. The first is a short story by Lev Bratishenko about the travails of a hapless Hungarian customs officer who is tasked with processing a 26,000-kilo truckload of cucumbers at the Romanian border. Bureaucratic absurdity ensues, most strikingly as he attempts to navigate the EU’s confounding regulations on acceptable produce dimensions. The second is the 1928 trial Brancusi vs United States, when Brancusi’s abstract sculpture Bird in Space was refused entry into the US as an artwork. Deemed not to conform to “imitations of natural objects, chiefly the human form,” the sculpture was classified under “Kitchen Utensils and Hospital Supplies” and charged a duty fee. Though both of these examples share themes of borders and the circulation of goods, they also touch on another kind of threshold: the limit of an object, or even an idea, and the collectivities who decide. In The Assistant, Kaiser’s own incarnated cucumbers stretch the limits of where an object’s fixed definition presses against
the exuberance of its own being. Amanda Sarroff
<br><br>
[link](https://openstudios2022.janvaneyck.nl/participants/marine-kaiser) 
