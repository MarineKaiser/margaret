+++
draft = true
date = "2015-06-01"

type = "projet"
layout= "un_carrousel" 
map = true

title = "Classification affective non universelle"
id = "canu"

lieu = [ "Bibliothèque publique communale des Riches-Claires, Bruxelles" ]
collaboration_avec = []
commanditaire =  []
credit = ""
remerciement = ""
Performeurs = ""


description_projet = ""
mot_cle_projet = []
auteur_du_projet = []
+++

Revoir la classification décimale universelle et reclasser les livres de la bibliothèque, par affinité. Les citations mutuelles comme affirmations d'intérêt et affection créent un nouvel agencement potentiel.

