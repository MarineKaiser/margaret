+++
draft = false
date = "2017-07-01"

type = "projet"
layout = "plusieurs_carrousels"
map = true

title = "Mutual Attraction"
id = "mutual_attraction"

lieu = []
collaboration_avec = [ "Mathias Pfund"]
commanditaire =  []
credit = "Photographies par Camille Charleux, Romain Duhamel, Fabrice Schneider et Camila Paolino"
remerciement = "Merci au Centre Culturel d’Uccle, tout particulièrement à Patrick Luypaert"
performeurs = ""

description_projet = ""
mot_cle_projet = []
auteur_du_projet = []
+++

Tentative de vol ou vandalisme ? Un arbre est tombé sur l’Abondance, sculpture publique de Joseph Witterwulghe. La statue est désormais cassée en deux, brisée net en biseau. Le temps de son indétermination, accueillir le fragment désolidarisé, remettre son image en circulation : faire une greffe. Faire une greffe avec notre porte-greffe en frigolite, soit la cape de pomme digérée en lava féminine à testicules. Dialogues des matériaux, les photographies permettent d’assurer la prise de la greffe sur le long terme.
