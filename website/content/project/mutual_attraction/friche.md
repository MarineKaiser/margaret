+++
draft = false
date = "2017-06-02"

type = "projet"
layout= "un_carrousel" 
map = false

title = "Catalogue Friche"
id = "friche"

lieu = []
collaboration_avec = []
commanditaire =  []
credit = ""
remerciement = ""
performeurs = ""


description_projet = ""
mot_cle_projet = []
auteur_du_projet = []


[[legende]]
	image = ["01"]
	contexte  =  []
	description = "Catalogue de l'exposition de fin de résidence organisée par le collectif Friche aux hangars de la Senne, Bruxelles du 6.03.2017 au 23.04.2017. <br> Avec Noémie Asper, Léa Belooussovitch, Justine Bougerol, Jules Bouteleux, Catharsis projection, Louis Darcel, Hannah de Corte, Benito Fuaro, Alix Hammond-Merchant, Antone Israel, Mathias Pfund et Marine Kaiser, Julie Larrouy et Claudia Radulescu, Yoojin Lee, Laure Lernoux, Gauthier Mentré et Vincent Gastout, Miss Machine, Rokko, Miyoshi, Elise Peroi, Bastien Poncelet, Johny Ripato, Félix Robin, Eva Schippers, Giulia Silvestri, Elsie Truxa."


+++


