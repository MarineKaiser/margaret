+++
draft = false
date = "2022-06-28"

type = "projet"
layout= "un_carrousel" 
map = true

title = "One Eye Open One Eye Closed"
subtitle = []
id = "eye"

lieu = []
collaboration_avec = []
commanditaire =  []
credit = "Photography : Jan van Eyck Academie"
remerciement = "Thanks to Bruno Alvez de Almeida, Ron Bernstein, Romy Finke, Aleksander Johan Andreassen"
performeurs = ""



+++

 A hole drilled through the wall in the shape of a closed eye offers an aperture to the complex relational character of our contemporary borders. Its title is a reference to a conversation Kaiser once had with a customs officer who told her, “Here we sometimes have to work with one eye open, one eye closed.”
