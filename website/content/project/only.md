+++
draft = false
date = "2022-05-29"

type = "projet"
layout= "un_carrousel" 
map = true

title = "The only thing that can affect magnetic inversion is bad news"
subtitle = []
id = "only"

lieu = []
collaboration_avec = []
commanditaire =  []
credit = "Photography : Nikos Staikoglou"
remerciement = "Thanks to Lisa Sudhibhasilp, Brent Dahl, Darling Services, Nikos Staikoglou, Hamid Soleymani, Ruby Hoette, Roberta Petzoldt, Paul John"
performeurs = ""



+++

On the highway to hell, call a space a spade, especially when a sparrow in the hand is better than the pigeon on the roof. In French, one would say call a cat a cat, but what’s lost in translation is whispered down the lane. By trial and error, step-by-step and maybe on one’s toes, a word spoken cannot be recalled. Lest we forget, it’s hard to remember the swan song. Off the beaten path, we are waiting for the snail-mail and on the tip of my tongue, the sentence pulled a vanishing act. Disappeared into thin air, the name of the game becomes the bird of passage. And once gone, we come to the end of the road. Lisa Sudhibhasilp

Marine Kaiser asked designer and screen printer Brent Dahl to not only print her work but also to freely design one sticker as an additional voice and perspective on this public object, which then took an unexpected detour. 
<br><br>
[r-e-c-l-a-m-e](https://www.r-e-c-l-a-m-e.nl/)
