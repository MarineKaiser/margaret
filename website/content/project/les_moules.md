+++
draft = false
date = "2018-05-01"

type = "projet"
layout= "un_carrousel" 
map = true

title = "Les Moules"
id = "les_moules"

lieu = []
collaboration_avec = [ "Mathias Pfund" ]
commanditaire =  []
credit = "Photographies par les Editions Cacahuète et Mathias Pfund"
remerciement = "Merci à Johana Blanc, Jessie Derogy, Marco Federico Cagnoni, Design Academy Eindhoven"
performeurs = ""

description_projet = ""
mot_cle_projet = []
auteur_du_projet = []
[[legende]]
	image = ["01"]
	contexte  =  [
		"Les Moules, édition de 115 moules thermoformés",
		"éditions Cacahuète, 8,5 x 5 cm"
		]
	description = "Chaque élément du multiple est unique et est pensé pour fonctionner lui-même comme une matrice.  Cent quinze variations autours du motif de la mouche écrasée s’introduisent dans le voisinage des cacahuètes et des glaçons."

+++
