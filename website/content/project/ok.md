+++
draft = true
date = "2014-06-01"

type = "projet"
layout= "un_carrousel" 
map = true

title = "OK"
id = "ok"

lieu = [ "HEAD, Genève" ]
collaboration_avec = []
commanditaire =  []
credit = ""
remerciement = ""
Performeurs = "Performance par Caroline Bourrit, Juliette Luginbuhl, Dorian Ozhan Sari"


description_projet = ""
mot_cle_projet = []
auteur_du_projet = []
+++

Trois performeurs arrivent, décidés. 
Ils montent en même temps sur un tabouret et en s’adressant aux spectateurs, lisent silencieusement.
Quelques minutes passées, un homme avec un accent entame le récit d'une histoire d’amour entre Marcel Broodthaers et Jean-Luc Godard, une femme manifeste le sous-texte, une femme chante en hauteurs de “euh” le poème de Mallarmé Un Coup de dès jamais n’abolira le hasard, barré ou souligné par Broodthaers. Ils portent les noms de leurs personnages.
Ils parlent au public, ils parlent seuls, ils parlent entre eux, ils parlent entre autres et leurs interventions terminées, ils s'en vont avec les tabourets.
