+++
draft = true
date = "2017-06-01"

type = "projet"
layout= "un_carrousel" 
map = false

title = "de printemps"
id = "de_printemps"

lieu = [ ]
collaboration_avec = [ "Stéphanie Verin" ]
commanditaire =  []
credit = ""
remerciement = ""
performeurs = ""


description_projet = ""
mot_cle_projet = []
auteur_du_projet = []
+++

Édition de 14 affiches A3 risographiées sur Munken 120gm/m2
Newsletter de nos exercices de pensée avec les pieds, les affiches constituent par fragments une fiction du vivre ensemble s’intéressant à l’échange de l’intime au public, la représentation des femmes, le mobilier urbain, la vie des plantes ainsi qu'aux personnages croisés au fil de nos balades célibataires.

