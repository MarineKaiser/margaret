+++
draft = false
date = "2017-07-01"

type = "projet"
layout = "plusieurs_carrousels"
map = true

title = "La ballade de Kaiser & Qasimov"
id = "la_ballade_de_kaiser_et_qasimov"

lieu = []
collaboration_avec = ["Stéphanie Verin"]
commanditaire =  []
credit = "Photographies par Loraine Furter, Ilaria Fantini et Sarah T.Brown"
remerciement = "Merci à Franck Ancel et Hamish Hosein"
performeurs = []

description_projet = ""
mot_cle_projet = []
auteur_du_projet = []


+++
Kaiser et Qasimov travaillent à se promener. Leurs conversations et leurs observations
parcourent l’espace public et proposent une fiction composée de
balades collectives, d’éditions, de conférences et de performances.
La représentation des femmes, l’aménagement urbain, les rencontres, la
relation entre l’intime et le public ainsi que le comportement des plantes sont
quelques éléments de leur reportage situé.
