+++
date = "2017-07-01"
draft = false

type = "projet"
layout= "un_carrousel" 
map = false

title = "Chéri"
id = "cheri"

lieu = []
collaboration_avec = [ "Stéphanie Verin" ]
commanditaire =  []
credit = ""
remerciement = ""
performeurs =  ""

[[legende]]
	image = ["01"]
	contexte  =  [
		"Chéri, 2017", 
		"Performance, 25min",
		"Les Grands-Voisins, Paris"
		]
	description = "Hé chérie t'as pas un 06? <br> A deux voix, la fiction de nos balades parcourants des espaces collectifs, semi-privés, publics, intimes, indéfinis et ses acteurs est rythmée d'images envoyées sur les téléphones du public, annoncées par une sonnerie analogique (triangle)."

+++