+++
draft = true
date = "2017-06-01"

type = "projet"
layout= "un_carrousel" 
map = false

title = "Le Retard"
id = "le_retard"

lieu = [ "Rendez-vous devant le centre culturel d'Uccle"]
collaboration_avec = [ "Stéphanie Verin" ]
commanditaire =  [ "Dans le cadre du festival Poisson-Evêque" ]
credit = ""
remerciement = ""
performeurs = ""

description_projet = ""
mot_cle_projet = []
auteur_du_projet = []
+++

Kaiser et Qasimov ont invité leurs invités a une balade collective. Ils ont marché et regardé ensemble la vie des sculptures, les assises publiques, l'entretien de la ville par ses employés et leurs histoires, des oeuvres de femmes et des noms d'hommes et de rues, du vert dans des espaces, la pratique des commerçants. Dans le parc, ils ont partagé une tarte puis se sont répartis les rôles du Débat des utopistes, une pièce de Bernadette Mayer détournant la parole de trente-six personnages historiques tels Sappho, Louisa May Alcott et le Sénateur 1.


