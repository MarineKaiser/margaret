+++
draft = false
date = "2017-06-01"

type = "projet"
layout= "un_carrousel" 
map = false

title = "La Présentation"
id = "la_presentation"

lieu = []
collaboration_avec = ["Stéphanie Verin"]
commanditaire =  []
credit = ""
remerciement = ""
performeurs = ""


description_projet = ""
mot_cle_projet = []
auteur_du_projet = []

[[legende]]
	image = ["01"]
	contexte  =  [
		"La Présentation, 2017", 
		"Conférence, performance, 25min", 
		"BMW brand store, Sergio Rossi, Square Marguerite Yourcenar"
		]
	description = "Invitées à donner une conférence par le master Multi de l’Académie royale des Beaux-Arts en résidence à l’Iselp, nous avons changé de porte et invité notre audience dans l’un des accueillants salons du BMW brand store attenant. Notre intérêt à détourner les espaces et à se les approprier temporairement ainsi qu’à regarder le dispositif (privé et public) riche d’histoires discrètes nous a conduit à nous/se/les présenter dans le BMW brand store. Nous nous sommes ensuite rendus dans le magasin de chaussures de luxe pour femmes Sergio Rossi afin d’en étudier le mobilier unique et identique à celui de Milan, de Jeddah, de Seoul, d’Almaty, de Makati City, de Moscou, de Paris et des autres; et avons terminé la balade par un échange dans le passage Marguerite Yourcenar."
	

+++