+++
date = "2017-04-01"
draft = false

type = "projet"
layout= "un_carrousel" 
map = false

title = "Le Confort"
id = "le_confort"

lieu = []
collaboration_avec = [ "Stéphanie Verin" ]
commanditaire =  []
credit = ""
remerciement = ""
performeurs =  ""

[[legende]]
	image = ["01"]
	contexte  =  [
		"Le confort, 2017", 
		"Lecture", 
		"Maison du monde, Dockx Bruxsel"
		]
	description = "Profitons de la chaleur du magasin pour lire le Traité de l’Unité universelle ou Traité de l’Association domestique agricole de Charles Fourier. Le centre commercial, lui même construit comme une galerie de cristal, fait face au familistère (forme d’habitat partagé organisé selon des principes de vie commune) construit par Jean-Baptiste Andrée Godin (d’après Fourier) en 1880 pour que ses travailleurs puisse disposer d’une vie d’entraide, d’échanges, de confort."


+++